package id.artivisi.training.telkomsigma.marketplace.discovery.trainingdiscovery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrainingDiscoveryApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrainingDiscoveryApplication.class, args);
	}
}
